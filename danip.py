import requests
import os

class DaniAlumnosApi:
    def __init__(self, base_url):
        self.base_url = base_url
        self.auth_token = None
        self.auth_user = None
        self.session = requests.Session()

    def get_token(self):
        url = f"{self.base_url}/sanctum/csrf-cookie"
        response = self.session.get(url)
        response.raise_for_status()

    def login(self, form):
        url = f"{self.base_url}/api/login"
        try:
            response = self.session.post(url, json=form)
            response.raise_for_status()
            data = response.json()
            if 'data' in data and 'token' in data['data']:
                self.auth_token = data['data']['token']
                self.auth_user = data['data']
                print(f"\nInicio de sesión exitoso. Usuario: {self.auth_user['nombre']}\n")
                os.system('cls' if os.name == 'nt' else 'clear')
            else:
                print("Error de inicio de sesión: 'token' no encontrado en la respuesta.")
                print(f"Respuesta: {data}")
        except requests.exceptions.RequestException as e:
            try:
                errors = e.response.json().get('errors', [])
                desc = ' '.join(errors)
                print(f"Error de inicio de sesión: {desc}")
            except ValueError:
                print(f"Error de inicio de sesión: {e.response.text}")

    def logout(self):
        self.auth_token = None
        self.auth_user = None
        print("Sesión cerrada exitosamente.")

    def register(self, form):
        url = f"{self.base_url}/api/usuarios"
        try:
            response = self.session.post(url, json=form)
            response.raise_for_status()
            data = response.json()
            print(f"Registro éxitoso: {data['message']}")
        except requests.exceptions.RequestException as e:
            try:
                errors = e.response.json().get('errors', [])
                desc = ' '.join(errors)
                print(f"Registro error: {desc}")
            except ValueError:
                print(f"Registro error: {e.response.text}")

    def GET(self):
        url = f"{self.base_url}/api/Alumnos"
        headers = {'Authorization': f"Bearer {self.auth_token}"}
        try:
            response = self.session.get(url, headers=headers)
            response.raise_for_status()
            alumnos = response.json()
            print(f"\n{'='*40}\nLista de alumnos:\n{'='*40}")
            for e in alumnos.get('data', []):
                print(f"\nID: {e.get('_id', 'N/A')}\nEscuela: {e.get('escuela', 'N/A')}\nMatricula: {e.get('matricula', 'N/A')}\nAlumno: {e.get('alumno', 'N/A')}\nCarrera: {e.get('carrera', 'N/A')}\nSemestre: {e.get('semestre', 'N/A')}\nFecha Nacimiento: {e.get('fecha_nacimiento', 'N/A')}\nFoto: {e.get('foto', 'N/A')}\n{'-'*40}")
            print(f"\n{'='*40}\n")
            return alumnos
        except requests.exceptions.RequestException as e:
            print(f"\n{'='*40}\nError al obtener alumnos : {e}\n{'='*40}\n")
            try:
                print(f"Detalles del error: {e.response.json()}\n")
            except ValueError:
                print(f"Detalles del error: {e.response.text}\n")

    def GET_ALUMNO(self, id):
        url = f"{self.base_url}/api/Alumnos/{id}"
        headers = {'Authorization': f"Bearer {self.auth_token}"}
        try:
            response = self.session.get(url, headers=headers)
            response.raise_for_status()
            alumno = response.json()
            e = alumno.get('data', {})
            print(f"\n{'='*40}\nAlumno ID: {id}\n{'='*40}")
            print(f"\nEscuela: {e.get('escuela', 'N/A')}\nMatricula: {e.get('matricula', 'N/A')}\nAlumno: {e.get('alumno', 'N/A')}\nCarrera: {e.get('carrera', 'N/A')}\nSemestre: {e.get('semestre', 'N/A')}\nFecha Nacimiento: {e.get('fecha_nacimiento', 'N/A')}\nFoto: {e.get('foto', 'N/A')}\n{'-'*40}")
            print(f"\n{'='*40}\n")
            return alumno
        except requests.exceptions.RequestException as e:
            print(f"\n{'='*40}\nError al obtener alumno: {e}\n{'='*40}\n")
            try:
                print(f"Detalles del error: {e.response.json()}\n")
            except ValueError:
                print(f"Detalles del error: {e.response.text}\n")

    def POST(self, form, image_path):
        url = f"{self.base_url}/api/Alumnos"
        headers = {'Authorization': f"Bearer {self.auth_token}"}

        if not os.path.isfile(image_path):
            print(f"\n{'='*40}\nEl archivo {image_path} no existe.\n{'='*40}\n")
            return

        if not image_path.lower().endswith(('.png', '.jpg', '.jpeg')):
            print("\n{'='*40}\nSolo se permiten archivos PNG y JPG.\n{'='*40}\n")
            return

        files = {'imagen': (os.path.basename(image_path), open(image_path, 'rb'), 'image/png' if image_path.lower().endswith('.png') else 'image/jpeg')}
        data = form

        try:
            print(f"\n{'='*40}\nCreando alumno con datos: {form}\n{'='*40}\n")
            response = self.session.post(url, json=data, files=files, headers=headers)
            response.raise_for_status()
            data = response.json()
            print(f"\n{'='*40}\nAlumno creado exitosamente: {data}\n{'='*40}\n")
            return data
        except requests.exceptions.RequestException as e:
            print(f"\n{'='*40}\nError al crear alumno: {e}\n{'='*40}\n")
            try:
                print(f"Detalles del error: {e.response.json()}\n")
            except ValueError:
                print(f"Detalles del error: {e.response.text}\n")

    def UPDATE(self, id, form):
        url = f"{self.base_url}/api/Alumnos/{id}"
        headers = {'Authorization': f"Bearer {self.auth_token}"}
        update_data = {key: value for key, value in form.items() if value is not None}
        try:
            response = self.session.put(url, json=update_data, headers=headers)
            response.raise_for_status()
            return response.json()
        except requests.exceptions.RequestException as e:
            print(f"\n{'='*40}\nError al actualizar Alumno: {e}\n{'='*40}\n")

    def DELETE(self, id):
        url = f"{self.base_url}/api/Alumnos/{id}"
        headers = {'Authorization': f"Bearer {self.auth_token}"}
        try:
            response = self.session.delete(url, headers=headers)
            response.raise_for_status()
            print(f"\n{'='*40}\nAlumno eliminado exitosamente: {response.json()}\n{'='*40}\n")
            return response.json()
        except requests.exceptions.HTTPError as e:
            if e.response.status_code == 500:
                print(f"\n{'='*40}\nError del servidor al eliminar alumno: {id}\n{'='*40}\n")
                try:
                    error_details = e.response.json()
                    print(f"Detalles del error: {error_details}\n")
                except ValueError:
                    print(f"Detalles del error: {e.response.text}\n")
            else:
                print(f"\n{'='*40}\nError al eliminar alumno: {e}\n{'='*40}\n")
                try:
                    error_details = e.response.json()
                    print(f"Detalles del error: {error_details}\n")
                except ValueError:
                    print(f"Detalles del error: {e.response.text}\n")
        except requests.exceptions.RequestException as e:
            print(f"\n{'='*40}\nError al eliminar alumno: {e}\n{'='*40}\n")


def clear_screen():
    os.system('clear' if os.name == 'posix' else 'cls')

def mostrar_menu(opciones):
    print('Seleccione una opción:')
    for clave in sorted(opciones, key=lambda x: int(x)):
        print(f' {clave}) {opciones[clave][0]}')

def leer_opcion(opciones):
    while True:
        opcion = input('Opción: ')
        if opcion in opciones:
            return opcion
        else:
            print('Opción incorrecta, vuelva a intentarlo.')

def ejecutar_opcion(opcion, opciones):
    opciones[opcion][1]()

def generar_menu(opciones, opcion_salida):
    opcion = None
    while opcion != opcion_salida:
        mostrar_menu(opciones)
        opcion = leer_opcion(opciones)
        ejecutar_opcion(opcion, opciones)
        print()

def menu_principal(api):
    opciones = {
        '1': ('Iniciar Sesión', lambda: iniciar_sesion(api)),
        '2': ('Registrar Usuario', lambda: registrar_usuario(api)),
        '3': ('Salir', salir)
    }
    generar_menu(opciones, '3')

def menu_operaciones(api):
    opciones = {
        '1': ('Ver Alumnos', lambda: ver_alumnos(api)),
        '2': ('Crear Alumno', lambda: crear_alumno(api)),
        '3': ('Actualizar Alumno', lambda: actualizar_alumno(api)),
        '4': ('Eliminar Alumno', lambda: eliminar_alumno(api)),
        '5': ('Cerrar Sesión', lambda: cerrar_sesion(api)),
        '6': ('Salir', salir)
    }
    generar_menu(opciones, '6')

def iniciar_sesion(api):
    correo = input("Correo: ")
    password = input("Contraseña: ")
    api.login({"correo": correo, "password": password})
    if api.auth_token:
        menu_operaciones(api)

def registrar_usuario(api):
    nombre = input("Nombre: ")
    correo = input("Correo: ")
    password = input("Contraseña: ")
    password_confirmation = input("Confirmar Contraseña: ")
    api.register({"nombre": nombre, "correo": correo, "password": password, "password_confirmation": password_confirmation})

def cerrar_sesion(api):
    api.logout()
    menu_principal(api)

def ver_alumnos(api):
    api.GET()

def crear_alumno(api):
    escuela = input("Escuela: ")
    matricula = input("Matricula: ")
    alumno = input("Alumno: ")
    carrera = input("Carrera: ")
    semestre = input("Semestre: ")
    fecha_nacimiento = input("Fecha Nacimiento (YYYY-MM-DD): ")
    foto = input("Ruta de la Foto: ")
    api.POST({
        "escuela": escuela,
        "matricula": matricula,
        "alumno": alumno,
        "carrera": carrera,
        "semestre": semestre,
        "fecha_nacimiento": fecha_nacimiento
    }, foto)

def actualizar_alumno(api):
    id = input("ID del Alumno a actualizar: ")
    api.GET_ALUMNO(id)
    escuela = input("Nueva Escuela: ")
    matricula = input("Nueva Matricula: ")
    alumno = input("Nuevo Alumno: ")
    carrera = input("Nueva Carrera: ")
    semestre = input("Nuevo Semestre: ")
    fecha_nacimiento = input("Nueva Fecha Nacimiento (YYYY-MM-DD): ")
    api.UPDATE(id, {
        "escuela": escuela if escuela else None,
        "matricula": matricula if matricula else None,
        "alumno": alumno if alumno else None,
        "carrera": carrera if carrera else None,
        "semestre": semestre if semestre else None,
        "fecha_nacimiento": fecha_nacimiento if fecha_nacimiento else None
    })

def eliminar_alumno(api):
    id = input("ID del Alumno a eliminar: ")
    api.DELETE(id)

def salir():
    print('Saliendo')
    exit()

if __name__ == '__main__':
    base_url = "https://alumnos-api-8.onrender.com"
    api = DaniAlumnosApi(base_url)
    menu_principal(api)
